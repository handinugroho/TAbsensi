import React, { Component } from 'react';
import Header from './components/Header';
import SideBar from './components/SideBar';
import Content from './components/Content';
import Login from './components/content/login';
import get from './components/content/config';
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import './components/content/css/AdminLTE.css';
import './components/content/css/_all-skins.min.css';
import './components/content/css/font-awesome.css';
import './components/content/css/font-awesome.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

class App extends Component {
  render() {
    //fungsi refresh token
    function refreshToken() {
      fetch(get.refreshtoken, {
        method: 'post',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          old_access_token: sessionStorage.name,
        })
      })
        .then(response => response.json())
        .then(response => {
          if (response.status === 0) {
            sessionStorage.clear()
            window.location.reload()
          }
          else {
            sessionStorage.setItem("name", response.token)
          }
        })
        .catch(error => {
          sessionStorage.clear()
          window.location.reload()
        })
    }

    // kalau token ga ada
    if ((sessionStorage.name === "undefined") || (sessionStorage.name === undefined)) {
      return (
          <Switch>
            <Route path="/login" component={Login} />
            <Redirect to="/login" />
          </Switch>
      )
    }

    //kalau token ada
    else {
      //fungsi untuk memanggil refresh token 1 detik setelah ada sesuatu yang di klik
      setTimeout(function () { refreshToken() }, 1000)

      return (
        <div>
          <Route path="/" component={Header} />
          <Route path="/" component={SideBar} />
          <Route path="/" component={Content} />
        </div>
      )
    }
  }
}

export default withRouter(App);