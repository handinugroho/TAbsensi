#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ArduinoJson.h>

byte next_kelas;
byte counter_next_kelas;

bool checkFile(fs::FS &fs, const char * path) {
  bool output;
  File file = fs.open(path);
  if (!file) {
    return false;
  }
  else {
    return true;
  }
}

bool checkDir(fs::FS &fs, const char * path) {
  bool output;
  File file = fs.open(path);
  if (!file.isDirectory()) {
    return false;
  }
  else {
    return true;
  }
}

void createDir(fs::FS &fs, const char * path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

String readFile(fs::FS &fs, const char * path) {
  String output;

  File file = fs.open(path);
  if (!file) {
    return "\0";
  }
  else {
    while (file.available()) {
      output = file.readStringUntil('\n');
    }
    file.close();
    return output;
  }
}

void writeFile(fs::FS &fs, const char * path, String message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void deleteFile(fs::FS &fs, const char * path) {
  Serial.printf("Deleting file: %s\n", path);
  if (fs.remove(path)) {
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}

void renameFile(fs::FS &fs, const char * path1, const char * path2) {
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

byte parse_json_durasi(const char* path)
{
  byte durasi;
  String read_sd;

  uint16_t size_response = 100;
  DynamicJsonDocument response_request(size_response);

  read_sd = readFile(SD, path);
  deserializeJson(response_request, read_sd);

  durasi = response_request["durasi"].as<byte>();
  return durasi;
}

void check_next_kelas()
{
  String read_sd;
  uint16_t size_response = 200;
  DynamicJsonDocument response_request(size_response);
  DateTime now = getTime();

  counter_next_kelas = next_kelas + 1;

  while (counter_next_kelas < 24) {
    if (checkDir(SD, ("/data/" + String(atoi(daysOfTheWeek[now.dayOfTheWeek()])) + "_" + String(counter_next_kelas)).c_str())) {
      read_sd = readFile(SD, ("/data/" + String(atoi(daysOfTheWeek[now.dayOfTheWeek()])) + "_" + String(counter_next_kelas) + "/0.txt").c_str());
      deserializeJson(response_request, read_sd);
      const char* matkul = response_request["kode_matkul"].as<const char*>();
      next_kode_matakuliah = "(" + String(counter_next_kelas) + ")" + String(matkul);
      break;
    }
    counter_next_kelas = counter_next_kelas + 1;
  }

  Serial.println("kelas berikutnya: " + String(counter_next_kelas));

  if (counter_next_kelas == 24) {
    next_kode_matakuliah = "    ------";
  }
  if (((counter_next_kelas - next_kelas) >= 2) && (now.hour() == next_kelas)) {
    update_data_from_ui = true;
  }
  else {
    update_data_from_ui = false;
  }
}
