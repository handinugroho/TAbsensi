#include "fsm.h"

TaskHandle_t Task1;
TaskHandle_t Task2;
TaskHandle_t Task3;
TaskHandle_t Task4;
TaskHandle_t Task5;

uint8_t a;
uint8_t fsm_state = 0;
bool task5update = false;

void TaskMainMatch(void * pvParameters);
void TaskUpdateData(void * pvParameters);
void TaskSendLog(void * pvParameters);
void TaskPingDevice(void * pvParameters);
void TaskUpdateDataFromUI(void * pvParameters);

void setup() {

  //Setting Serial
  Serial.begin(115200);
  Serial.print("awal: ");
  Serial.println(ESP.getFreeHeap());

  //Setting lcd
  lcd.init();
  lcd.backlight();
  lcd.createChar(0, customLoading);
  lcd.createChar(2, customBaterai20);
  lcd.createChar(3, customBaterai40);
  lcd.createChar(4, customBaterai60);
  lcd.createChar(5, customBaterai80);
  lcd.createChar(6, customBaterai100);
  lcd.createChar(7, customCharging);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(" Starting device... ");
  delay(2000);
  Serial.print("lcd: ");
  Serial.println(ESP.getFreeHeap());

  //Setting Ethernet
  Ethernet.init(4);
  Serial.print("ethernet: ");
  Serial.println(ESP.getFreeHeap());

  //Setting Wifi
  WiFi.begin(ssid, password);
  Serial.print("wifi: ");
  Serial.println(ESP.getFreeHeap());
  //  while (WiFi.status() != WL_CONNECTED) {
  //    delay(500);
  //    Serial.print(".");
  //  }

  //Setting SD Card
  while (!SD.begin(5)) {
    Serial.println("Card Mount Failed");
    lcd.setCursor(0, 2);
    lcd.print(" SD card not found. ");
  }
  Serial.print("sd: ");
  Serial.println(ESP.getFreeHeap());

  //Setting Fingerprint
  finger.begin(57600);
  while (!finger.verifyPassword()) {
    Serial.println("Did not find fingerprint sensor :(");
    lcd.setCursor(0, 2);
    lcd.print(" Fingerprint Error. ");
  }
  Serial.print("fingerprint: ");
  Serial.println(ESP.getFreeHeap());

  //Setting RTC
  while (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    lcd.setCursor(0, 2);
    lcd.print("   RTC not found    ");
  }
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    lcd.setCursor(0, 3);
    lcd.print("   RTC lost power   ");
    delay(2000);
    // following line sets the RTC to the date & time this sketch was compiled
    //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  Serial.print("rtc: ");
  Serial.println(ESP.getFreeHeap());
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  //rtc.adjust(DateTime(2020, 1, 21, 13, 39, 0));//year,month,day,hour,minute,second

  DateTime now = getTime();
  String date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second());
  Serial.println(date);

  //setting buzzer
  pinMode(buzzer, OUTPUT);

  //setting indikator baterai
  pinMode(indikator_baterai, INPUT);

  //input kode ruangan;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("   No Error Found   ");
  delay(2000);


  finger.emptyDatabase();

  if (checkDir(SD, "/data") == false) {
    createDir(SD, "/data");
  }
  if (checkDir(SD, "/data2") == false) {
    createDir(SD, "/data2");
  }
  if (checkDir(SD, "/log") == false) {
    createDir(SD, "/log");
  }
  if (checkFile(SD, "/log/log_all.txt") == false) {
    writeFile(SD, "/log/log_all.txt", "\0");
  }
  if (checkFile(SD, "/log/counter_file_send_log.txt") == false) {
    writeFile(SD, "/log/counter_file_send_log.txt", "{\"count\":\"0\"}");
  }

  while (Downloaded_Data == false) {
    String read_kode_ruangan;

    lcd.setCursor(0, 0);
    lcd.print("Input Kode Ruangan: ");
    lcd.setCursor(0, 2);

    if (checkFile(SD, "/kode_ruangan.txt")) {
      read_kode_ruangan = readFile(SD, "/kode_ruangan.txt");
      lcd.setCursor(0, 3);
      lcd.print(" Automatic In: " + read_kode_ruangan + " ");
    }
    else {
      read_kode_ruangan = "\0";
    }
    kode_ruangan = "\0";
    //    kode_ruangan = "9125";
    while (kode_ruangan == NULL) {
      waitForKeypad(&kode_ruangan, &a);
      if ((no_key_10 == true) && (read_kode_ruangan != NULL)) {
        kode_ruangan = read_kode_ruangan;
      }
    }
    if (read_kode_ruangan == NULL) {
      getDataFromServer_first();
    }
    else {
      // pengecekan kode ruangan sama atau engga
      if (read_kode_ruangan != kode_ruangan) {
        getDataFromServer_first();
      }
      else {
        Downloaded_Data = true;
        if ((Ethernet.linkStatus() == LinkON) || (WiFi.status() == WL_CONNECTED)) {
          pingDeviceAwal();
        }
      }
    }
  }

  kelasSekarang();
  Serial.println(ESP.getFreeHeap());

  lcd.clear();
  showBatreMatkul();

  xTaskCreatePinnedToCore(TaskMainMatch,  "TaskMainMatch",  6000, NULL, 1, &Task1, 1);
  xTaskCreatePinnedToCore(TaskUpdateData, "TaskUpdateData", 5000, NULL, 2, &Task2, 1);
  xTaskCreatePinnedToCore(TaskSendLog,  "TaskSendLog",  5000, NULL, 1, &Task3, 0);
  xTaskCreatePinnedToCore(TaskPingDevice,  "TaskPingDevice",  5000, NULL, 3, &Task4, 1);
  xTaskCreatePinnedToCore(TaskUpdateDataFromUI,  "TaskUpdateDataFromUI",  7000, NULL, 4, &Task5, 1);
  vTaskSuspend(Task5);
}

void TaskMainMatch(void * pvParameters)  // This is a task.
{
  char keypressed;
  for (;;)
  {
    vTaskDelay(20);
    DateTime now = getTime();
    if ((now.minute() % 1 == 0) && (now.second() < 1)) {
      showBatreMatkul();
    }
    if (now.second() % 1 == 0) {
      char buff[] = "DD-MM-YYYY  hh:mm:ss";
      lcd.setCursor(0, 0);
      lcd.print(now.toString(buff));
    }

    keypressed = myKeypad.getKey();
    if ((keypressed == 'A') || (keypressed == 'B') || (keypressed == 'C')) {
      key_mode = keypadMode(keypressed);
    }
    if (fsm_state != 0) {
      vTaskSuspend(Task2);
      vTaskSuspend(Task3);
      vTaskSuspend(Task4);
      lcd.clear();
      fsm(&key_mode, &fsm_state);
      showBatreMatkul();
      vTaskResume(Task2);
      vTaskResume(Task3);
      vTaskResume(Task4);
    }
    else {
      fsm(&key_mode, &fsm_state);
    }
  }
}
void TaskUpdateData( void * pvParameters ) {
  String next_matkul;
  byte menit;
  for (;;) {
    Serial.println("Task2");
    Serial.println(ESP.getFreeHeap());
    Serial.println(next_kelas);
    DateTime now = getTime();
    if ((now.minute() >= 40) && (now.minute() < 60)) {
      if ((now.hour() == next_kelas) && (checkDir(SD, ("/data/" + String(atoi(daysOfTheWeek[now.dayOfTheWeek()])) + "_" + String((now.hour() + 1))).c_str()))) {
        vTaskSuspend(Task3);
        vTaskSuspend(Task1);

        uploadTemplateFingerprint("/data/" + String(daysOfTheWeek[now.dayOfTheWeek()]) + "_" + String(now.hour() + 1));
        speedBuzzer(500, 1);

        next_kelas = now.hour() + parse_json_durasi(("/data/" + String(atoi(daysOfTheWeek[now.dayOfTheWeek()])) + "_" + String(now.hour() + 1) + "/0.txt").c_str());
        Serial.println("Updated");
        check_next_kelas();
        getFinger = true;
        showBatreMatkul();

        vTaskResume(Task3);
        vTaskResume(Task1);
      }
      else if ((now.hour() < (uint8_t)next_kelas) && ((uint8_t)next_kelas < 23)) {
        Serial.println(next_kelas);
      }
      else {
        next_kelas = now.hour() + 1;
        check_next_kelas();
        getFinger = false;
        kode_matakuliah = "------";
        finger.emptyDatabase();
        Serial.println("Tidak ada kelas");
      }
    }
    vTaskDelay(180000);
  }
}

void TaskSendLog(void * pvParameters)
{
  for (;;)
  {
    vTaskDelay(5000);
    sendLog();
  }
}

void TaskPingDevice(void * pvParameters)
{
  uint16_t size_response = 256;
  DynamicJsonDocument response_request(size_response);
  for (;;)
  {
    vTaskDelay(240000);
    Serial.println("Task4");
    response_request = httpPostRequest(size_response, server, address_url, "/ruangan/update_last_seen", ("kodedevice=" + kode_device));
    if (response_request.isNull()) {
      Serial.println("Request Failed!");
    }
    else {
      if (response_request["status"].as<byte>() == 1) {
        token = response_request["token"].as<String>();
        Serial.println("update lastseen");
      }
      else if (response_request["status"].as<byte>() == 2) {
        token = response_request["token"].as<String>();
        if (task5update == false) {
          Serial.println("Update data found");
          task5update = true;
          vTaskResume(Task5);
        }
      }
      else {
        Serial.println("Update Failed");
      }
    }
  }
}

void TaskUpdateDataFromUI(void * pvParameters)
{
  uint16_t size_response = 256;
  DynamicJsonDocument response_request(size_response);
  for (;;)
  {
    vTaskDelay(300000);
    Serial.println("Task5");
    check_next_kelas();
    DateTime now = getTime();
    if ((update_data_from_ui) && ((now.minute() >= 4) && (now.minute() <= 60))) { //
      Serial.println("Updating new data");
      vTaskSuspend(Task1);
      vTaskSuspend(Task2);
      vTaskSuspend(Task3);
      vTaskSuspend(Task4);
      lcd.clear();
      storetoSDCard();
      if (Downloaded_Data == false) {
        delay(2000);
        Serial.println("failed download data");
        vTaskResume(Task2);
        vTaskResume(Task3);
        vTaskResume(Task4);
        vTaskResume(Task1);
      }
      else {
        Serial.println("Update Data");
        response_request = httpPostRequest(size_response, server, address_url, "/ruangan/update_device", ("kodedevice=" + kode_device + "&kode=1"));
        if (response_request.isNull()) {
          Serial.println("Request Failed!");
          vTaskResume(Task2);
          vTaskResume(Task3);
          vTaskResume(Task4);
          vTaskResume(Task1);
        }
        else {
          if (response_request["status"].as<byte>() == 1) {
            Serial.println("updated task5");
            task5update = false;
            update_data_from_ui = false;
            kelasSekarang();
            lcd.clear();
            showBatreMatkul();
            vTaskResume(Task2);
            vTaskResume(Task3);
            vTaskResume(Task4);
            vTaskResume(Task1);
            vTaskSuspend(NULL);
          }
          else {
            Serial.println("Update Failed");
            vTaskResume(Task2);
            vTaskResume(Task3);
            vTaskResume(Task4);
            vTaskResume(Task1);
          }
        }
      }
    }
  }
}

void loop()
{
}
//2520 line 15/03/2020

