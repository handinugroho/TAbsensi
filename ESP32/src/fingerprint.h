#include "setting.h"
#include "sd_card.h"
#include "keypad_lib.h"

uint8_t p ;

void kelasSekarang();
void showBatreMatkul();

uint8_t deleteFingerprint(uint8_t id) {

  p = finger.deleteModel(id);

  if (p == FINGERPRINT_OK) {
    Serial.println("Deleted!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not delete in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.print("Unknown error: 0x"); Serial.println(p, HEX);
    return p;
  }
}

uint8_t enrollFingerprint() {
  byte counter_finger; // 1 atau 2
  byte check_finger; // 1 finger match, 2 finger did not match, 3 finger berhasil dikirim, 4 finger ga berhasil dikirim, 5 ga ada finger sampai 10 detik
  byte start_time, limit_time;

  for (counter_finger = 0; counter_finger < 2; counter_finger++) {
    p = -1;
    check_finger = 2;

    while (check_finger == 2) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("---   Register   ---");
      lcd.setCursor(0, 1);
      lcd.print("      Finger " + String(counter_finger + 1) + "      ");
      lcd.setCursor(0, 2);
      lcd.print("Waiting for finger..");
      start_time = start_time_second();
      limit_time = start_time + 10;
      while ((p != FINGERPRINT_OK) && (start_time < limit_time)) {
        p = finger.getImage();
        DateTime now = getTime();
        switch (p) {
          case FINGERPRINT_OK:
            Serial.println("\nImage taken");
            break;
          case FINGERPRINT_NOFINGER:
            Serial.print(",");
            if (limit_time > 48) {
              start_time = now.second();
            }
            else {
              start_time = start_time_second();
            }
            break;
          case FINGERPRINT_PACKETRECIEVEERR:
            Serial.println("Communication error");
            break;
          case FINGERPRINT_IMAGEFAIL:
            Serial.println("Imaging error");
            break;
          default:
            Serial.println("Unknown error");
            break;
        }
      }

      if (start_time >= limit_time) {
        check_finger = 5;
        Serial.println("break enroll");
        break;
      }

      // OK success!
      if (p == FINGERPRINT_OK) {
        p = finger.image2Tz(1);
        switch (p) {
          case FINGERPRINT_OK:
            Serial.print("Remove finger ");
            lcd.setCursor(0, 2);
            lcd.print("     Get Finger     ");
            lcd.setCursor(0, 3);
            lcd.print(" Remove your finger ");
            delay(1000);
            Serial.println("Image converted");
            break;
          case FINGERPRINT_IMAGEMESS:
            Serial.println("Image too messy");
            return p;
          case FINGERPRINT_PACKETRECIEVEERR:
            Serial.println("Communication error");
            return p;
          case FINGERPRINT_FEATUREFAIL:
            Serial.println("Could not find fingerprint features");
            return p;
          case FINGERPRINT_INVALIDIMAGE:
            Serial.println("Could not find fingerprint features");
            return p;
          default:
            Serial.println("Unknown error");
            return p;
        }
        p = 0;
        while (p != FINGERPRINT_NOFINGER) {
          p = finger.getImage();
        }

        p = -1;
        lcd.setCursor(0, 2);
        lcd.print("  Place same your   ");
        lcd.setCursor(0, 3);
        lcd.print("   finger again..   ");
        Serial.println("Place same finger again");
        start_time = start_time_second();
        limit_time = start_time + 10;
        while ((p != FINGERPRINT_OK) && (start_time < limit_time)) {
          p = finger.getImage();
          DateTime now = getTime();
          switch (p) {
            case FINGERPRINT_OK:
              Serial.println("\nImage taken");
              break;
            case FINGERPRINT_NOFINGER:
              if (limit_time > 48) {
                start_time = now.second();
              }
              else {
                start_time = start_time_second();
              }
              Serial.print(".");
              break;
            case FINGERPRINT_PACKETRECIEVEERR:
              Serial.println("Communication error");
              break;
            case FINGERPRINT_IMAGEFAIL:
              Serial.println("Imaging error");
              break;
            default:
              Serial.println("Unknown error");
              break;
          }
        }

        if (start_time >= limit_time) {
          check_finger = 5;
          Serial.println("break enroll2");
          break;
        }

        p = finger.image2Tz(2);
        switch (p) {
          case FINGERPRINT_OK:
            lcd.setCursor(0, 2);
            lcd.print("     Get Finger     ");
            lcd.setCursor(0, 3);
            lcd.print(" Remove your finger ");
            Serial.println("Image converted");
            delay(2000);
            break;
          case FINGERPRINT_IMAGEMESS:
            Serial.println("Image too messy");
            return p;
          case FINGERPRINT_PACKETRECIEVEERR:
            Serial.println("Communication error");
            return p;
          case FINGERPRINT_FEATUREFAIL:
            Serial.println("Could not find fingerprint features");
            return p;
          case FINGERPRINT_INVALIDIMAGE:
            Serial.println("Could not find fingerprint features");
            return p;
          default:
            Serial.println("Unknown error");
            return p;
        }

        // OK converted!
        Serial.print("Creating model for #");  //Serial.println(id);
        while (p != FINGERPRINT_NOFINGER) {
          p = finger.getImage();
        }

        p = finger.createModel();
        if (p == FINGERPRINT_OK) {
          Serial.println("Prints matched!");
          p = 1;
          lcd.setCursor(0, 2);
          lcd.print("   Prints matched   ");
          lcd.setCursor(0, 3);
          lcd.print(" Upload your finger ");
          delay(2000);
          check_finger = 1;
        } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
          Serial.println("Communication error");
          return p;
        } else if (p == FINGERPRINT_ENROLLMISMATCH) {
          Serial.println("Fingerprints did not match");
          lcd.setCursor(0, 2);
          lcd.print("Finger did not match");
          lcd.setCursor(0, 3);
          lcd.print("     Try again      ");
          delay(2000);
          check_finger = 2;
        } else {
          Serial.println("Unknown error");
          return p;
        }

        if (check_finger == 1) {
          Serial.print("==> Attempting to get Templete #"); //Serial.println(id);
          p = finger.getModel();
          switch (p) {
            case FINGERPRINT_OK:
              Serial.print("Template ");
              //Serial.print(id);
              Serial.println(" transferring:");
              break;
            default:
              Serial.print("Unknown error "); Serial.println(p);
              return p;
          }

          uint8_t bytesReceived[900];

          short i = 0;
          String template_finger = "\0";
          while (i <= 554 ) {
            if (Serial2.available()) {
              bytesReceived[i++] = Serial2.read();
            }
          }

          Serial.println("Decoding packet...");

          // Filtering The Packet
          short a = 0;
          for (short i = 10; i <= 554; ++i) {
            a++;
            if (a >= 129)
            {
              i += 10;
              a = 0;
            }
            else
            {
              if (i == 554) {
                template_finger = template_finger + bytesReceived[i - 1];
              }
              else {
                template_finger = template_finger + bytesReceived[i - 1] + ", ";
              }
            }
          }
          Serial.print("Template data: ");
          Serial.println(template_finger);
          Serial.println("Uploading data to database...");

          String request_body = "device=" + kode_device + "&template=" + template_finger;

          uint16_t size_response = 2100;
          DynamicJsonDocument response_request(size_response);
          response_request = httpPostRequest(size_response, server, address_url, "/fingerprint/create", request_body);
          if (response_request.isNull()) {
            Serial.println("Request Failed!");
            lcd.setCursor(0, 2);
            lcd.print("   Upload failed    ");
            lcd.setCursor(0, 3);
            lcd.print("  Connection lost   ");
            delay(2000);
            check_finger = 4;
          }
          else {
            if (response_request["status"].as<byte>() == 1) {
              Serial.println("Upload success");
              lcd.setCursor(0, 2);
              lcd.print("--------------------");
              lcd.setCursor(0, 3);
              lcd.print("-- Upload success --");
              delay(2000);
              check_finger = 3;
            }
            else {
              Serial.println("Upload failed");
              lcd.setCursor(0, 2);
              lcd.print("   Upload failed    ");
              lcd.setCursor(0, 3);
              lcd.print("  Connection lost   ");
              delay(2000);
              check_finger = 4;
            }
          }
        }
      }
    }
    if ((check_finger == 4) || (check_finger == 5)) {
      break;
    }
  }
}

uint8_t uploadTemplateToFlash(char* str, uint16_t id) {
  uint8_t packet2[128];
  uint8_t packet3[128];
  uint8_t packet4[128];
  uint8_t packet5[128];

  char* temp = strtok(str, ",");
  for (short i = 0; i < 512; i++) {
    if (i < 128) {
      packet2[i] = (uint8_t)atoi(temp);
    }
    else if (i < 256) {
      packet3[i - 128] = (uint8_t)atoi(temp);
    }
    else if (i < 384) {
      packet4[i - 256] = (uint8_t)atoi(temp);
    }
    else if (i < 512) {
      packet5[i - 384] = (uint8_t)atoi(temp);
    }
    temp = strtok (NULL, ",");
  }

  //Serial.print("\n===> Write Packet");
  p = finger.uploadModelZFM20(packet2, packet3, packet4, packet5);   // Simpan di Char Buffer 01
  switch (p) {
    case FINGERPRINT_OK:
      //Serial.println(" SUKSES");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_BADPACKET:
      Serial.println("Bad packet");
      return p;
    default:
      {
        Serial.println("\n==>[SUKSES] UploadModel = ");
        //return p;
      }
  }
  Serial.print("ID = "); Serial.print(id);
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    Serial.println(" Stored!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }
}

void uploadTemplateFingerprint(String path) {
  Serial.println("Start.");
  finger.emptyDatabase();
  Serial.println(kode_ruangan);
  String read_sd_to_finger_config, read_sd_to_finger_data, bagian;
  short i, j, k, l, counter_file, last_counter, loading_lcd, count;
  float counter_loading_lcd;
  byte m, count_file;
  const char* durasi_config;
  const char* kodematkul_config;
  const char* kelas_config;
  const char* count_pengguna_config;
  const char* count_dosen_config;
  const char* count_asisten_config;

  read_sd_to_finger_config = readFile(SD, (path + "/0.txt").c_str());

  uint16_t size_response1 = 256;
  DynamicJsonDocument response_request(size_response1);
  deserializeJson(response_request, read_sd_to_finger_config);

  if (response_request.isNull()) {
    Serial.println("Failed read SD card");
  }
  else {
    durasi_config = response_request["durasi"].as<const char*>();
    kodematkul_config = response_request["kode_matkul"].as<const char*>();
    kelas_config = response_request["kelas"].as<const char*>();
    count_pengguna_config = response_request["count_pengguna"].as<const char*>();
    count_dosen_config = response_request["count_dosen"].as<const char*>();
    count_asisten_config = response_request["count_asisten"].as<const char*>();

    counter_dosen = atoi((char*)count_dosen_config);
    counter_asisten = atoi((char*)count_asisten_config);
    kode_matakuliah = String((char*)kodematkul_config);
    kelas = String((char*)kelas_config);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("--- Update Kelas ---");
    lcd.setCursor(0, 1);
    lcd.print(" " + String(kodematkul_config) + "  " + String(kelas_config) + "  " + String(durasi_config) + " Jam ");

    k = 1;
    count_file = 1;
    for (m = 0; m < 3; m++) { //0 -> dosen, 1-> asisten, 2->pengguna
      lcd.setCursor(0, 3);
      lcd.print("                    ");
      if (m == 0) {
        count = atoi((char*)count_dosen_config);
        bagian = "Dosen";
        lcd.setCursor(0, 2);
        lcd.print("    Update Dosen    ");
      }
      else if (m == 1) {
        count = atoi((char*)count_asisten_config);
        bagian = "Asisten";
        lcd.setCursor(0, 2);
        lcd.print("   Update Asisten   ");
      }
      else {
        count = atoi((char*)count_pengguna_config);
        bagian = "Pengguna";
        lcd.setCursor(0, 2);
        if (atoi((char*)count_pengguna_config) >= 100) {
          lcd.print("   " + String(count_pengguna_config) + " Mahasiswa    ");
        }
        else {
          lcd.print("    " + String(count_pengguna_config) + " Mahasiswa    ");
        }
      }

      counter_file = count / 5;

      if (count % 5 == 0) {
        counter_file = counter_file;
      }
      else {
        counter_file = counter_file + 1;
        last_counter = count % 5;
      }

      if (counter_file == 0) {
        Serial.println("Data " + bagian + " " + String(kodematkul_config) + " Tidak ditemukan");
      }
      else {
        loading_lcd = ceil(20 / counter_file);
        counter_loading_lcd = 1;
        for (i = 1; i < (counter_file + 1); i++) {
          for ( l = 0; l < (loading_lcd + 1); l++) {
            lcd.setCursor(floor(counter_loading_lcd) + l - 1, 3);
            if ((floor(counter_loading_lcd) + l - 1) < 20) {
              lcd.write(0);
            }
          }
          counter_loading_lcd = counter_loading_lcd + float(20.0 / float(counter_file));

          read_sd_to_finger_data = readFile(SD, (path + "/" + String(count_file) + ".txt").c_str());

          uint16_t size_response2 = 25000;
          DynamicJsonDocument response_request(size_response2);
          deserializeJson(response_request, read_sd_to_finger_data);
          if (response_request.isNull()) {
            Serial.println("Failed read File-" + String(i) + " from SD card");
          }
          else {
            if ((i == counter_file) && (count % 5 != 0)) {
              for (j = 0; j < last_counter; j++) {
                const char* sd_card_nim = response_request["hasil"][j]["nim"].as<const char*>();
                const char* sd_card_nama = response_request["hasil"][j]["nama"].as<const char*>();
                nim[k] = String((char*)sd_card_nim);
                nama[k] = String((char*)sd_card_nama);

                const char* sd_card_finger1 = response_request["hasil"][j]["finger1"].as<const char*>();
                const char* sd_card_finger2 = response_request["hasil"][j]["finger2"].as<const char*>();
                uint16_t id_finger1 = (uint16_t)((k * 2) - 1);
                uint16_t id_finger2 = (uint16_t)(k * 2);

                uploadTemplateToFlash((char*)sd_card_finger1, id_finger1);
                uploadTemplateToFlash((char*)sd_card_finger2, id_finger2);

                k = k + 1;
              }
            }
            else {
              for (j = 0; j < 5; j++) {
                const char* sd_card_nim = response_request["hasil"][j]["nim"].as<const char*>();
                const char* sd_card_nama = response_request["hasil"][j]["nama"].as<const char*>();
                nim[k] = String((char*)sd_card_nim);
                nama[k] = String((char*)sd_card_nama);

                const char* sd_card_finger1 = response_request["hasil"][j]["finger1"].as<const char*>();
                const char* sd_card_finger2 = response_request["hasil"][j]["finger2"].as<const char*>();
                uint16_t id_finger1 = (uint16_t)((k * 2) - 1);
                uint16_t id_finger2 = (uint16_t)(k * 2);

                uploadTemplateToFlash((char*)sd_card_finger1, id_finger1);
                uploadTemplateToFlash((char*)sd_card_finger2, id_finger2);

                k = k + 1;
              }
            }
          }
          count_file = count_file + 1;
        }
      }
    }
  }

  //check siapa aja yg udah absen (biar ga absen 2x) ada di file namanya absen_config sama absen_done,
  //absen_config berfungsi untuk mengecek apakah absen_done itu masih berlaku
  //absen_done berfungsi mencatat array berapa saja yang sudah absen
  String read_config, read_absen_all;
  //  char* read_absen_single;

  DateTime now = getTime();
  String date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day());

  if (checkFile(SD, "/absen_config.txt")) {
    read_config = readFile(SD, "/absen_config.txt");
    DynamicJsonDocument response_request(size_response1);
    deserializeJson(response_request, read_config);

    const char* absen_date_config = response_request["date"].as<const char*>();
    const char* absen_name_folder_config = response_request["name_folder"].as<const char*>();
    const char* absen_kode_matkul_config = response_request["kode_matkul"].as<const char*>();
    const char* absen_kelas_config = response_request["kelas"].as<const char*>();

    if ((date == absen_date_config) && (path == absen_name_folder_config) && (String(kodematkul_config) == absen_kode_matkul_config) && (String(kelas_config) == absen_kelas_config)) {
      read_absen_all = readFile(SD, "/absen_done.txt");
      Serial.print("Sudah Absen: ");
      Serial.println(read_absen_all);
    }
    else {
      deleteFile(SD, "/absen_config.txt");
      deleteFile(SD, "/absen_done.txt");
      writeFile(SD, "/absen_config.txt", "{\"date\":\"" + date + "\",\"name_folder\":\"" + path + "\",\"kode_matkul\":\"" + kodematkul_config + "\",\"kelas\":\"" + kelas_config + "\"}");
      writeFile(SD, "/absen_done.txt", "\0");
    }
  }
  else {
    writeFile(SD, "/absen_config.txt", "{\"date\":\"" + date + "\",\"name_folder\":\"" + path + "\",\"kode_matkul\":\"" + kodematkul_config + "\",\"kelas\":\"" + kelas_config + "\"}");
    writeFile(SD, "/absen_done.txt", "\0");
  }


  lcd.clear();
  Serial.println("Done.");
}

void upload_fingerprint_log(String nim, String nama, String kode_ruangan, String kode_matakuliah, String kelas, String statuss) {
  int counter_file;
  uint16_t size_response;
  byte response_count;

  String date, data_log, read_counter;

  DateTime now = getTime();
  date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second());
  Serial.println("Match at " + date);

  data_log = "waktu=" + date + "&nim=" + nim + "&nama=" + nama + "&koderuangan=" + kode_ruangan + "&kodematkul=" + kode_matakuliah + "&kelas=" + kelas + "&status=" + statuss + "&keterangan=Hadir,";

  appendFile(SD, "/log/log_all.txt", data_log.c_str());

  counter_file = 0;
  while (1) {
    counter_file = counter_file + 1;
    if (checkFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str()) == false) {
      writeFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str(), "{\"count\":\"1\"}");
      writeFile(SD, ("/log/send_log_" + String(counter_file) + ".txt").c_str(), data_log.c_str());
      break;
    }
    else {
      read_counter = readFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str());
      size_response = 256;
      DynamicJsonDocument counter(size_response);
      deserializeJson(counter, read_counter);
      response_count = counter["count"].as<byte>();
      if (response_count < 100) {
        writeFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str(), "{\"count\":\"" + String(response_count + 1) + "\"}");
        appendFile(SD, ("/log/send_log_" + String(counter_file) + ".txt").c_str(), data_log.c_str());
        break;
      }
    }
  }
  writeFile(SD, "/log/counter_file_send_log.txt", "{\"count\":\"" + String(counter_file) + "\"}");
}

void sendLog() {
  String read_log, read_counter_file, read_counter_log;
  char* send_log;
  char* update_log;
  int response_count_file, counter_file;
  uint16_t size_response;
  byte response_count;

  read_counter_file = readFile(SD, "/log/counter_file_send_log.txt");
  size_response = 256;
  DynamicJsonDocument counter_file_log(size_response);
  deserializeJson(counter_file_log, read_counter_file);
  response_count_file = counter_file_log["count"].as<int>();

  counter_file = response_count_file;

  while (counter_file != 0) {
    if (checkFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str())) {
      read_counter_log = readFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str());
      size_response = 256;
      DynamicJsonDocument counter(size_response);
      deserializeJson(counter, read_counter_log);
      response_count = counter["count"].as<byte>();
      if (response_count > 0) {
        break;
      }
      else {
        deleteFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str());
        deleteFile(SD, ("/log/send_log_" + String(counter_file) + ".txt").c_str());
        writeFile(SD, "/log/counter_file_send_log.txt", "{\"count\":\"" + String(counter_file - 1) + "\"}");
        counter_file = counter_file - 1;
      }
    }
    else {
      counter_file = 0;
      break;
    }
  }

  if (counter_file != 0) {
    read_log = readFile(SD, ("/log/send_log_" + String(counter_file) + ".txt").c_str());
    send_log = strtok((char*)read_log.c_str(), ",");
    update_log = strtok(NULL, "\0");

    size_response = 256;
    DynamicJsonDocument response_request(size_response);
    response_request = httpPostRequest(size_response, server, address_url, "/log/create", String(send_log));

    if (response_request.isNull()) {
      Serial.println("Request Failed!");
    }
    else {
      if (response_request["status"].as<byte>() == 1) {
        writeFile(SD, ("/log/counter_send_log_" + String(counter_file) + ".txt").c_str(), "{\"count\":\"" + String(response_count - 1) + "\"}");
        writeFile(SD, ("/log/send_log_" + String(counter_file) + ".txt").c_str(), String(update_log));
      }
      else {
        Serial.println("Upload failed");
      }
    }
  }
}

uint16_t matchFingerprint() {
  p = -1;
  if (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
      case FINGERPRINT_OK:
        Serial.println("\nImage taken");
        break;
      case FINGERPRINT_NOFINGER:
        //Serial.print(",");
        return nofinger;
      //break;
      case FINGERPRINT_PACKETRECIEVEERR:
        Serial.println("Communication error");
        return nofinger;
      case FINGERPRINT_IMAGEFAIL:
        Serial.println("Imaging error");
        return nofinger;
      default:
        Serial.println("Unknown error");
        return nofinger;
    }
  }

  if (p == FINGERPRINT_OK) {
    p = finger.image2Tz();
    switch (p) {
      case FINGERPRINT_OK:
        Serial.println("Image converted");
        break;
      case FINGERPRINT_IMAGEMESS:
        Serial.println("Image too messy");
        return 0;
      case FINGERPRINT_PACKETRECIEVEERR:
        Serial.println("Communication error");
        return 0;
      case FINGERPRINT_FEATUREFAIL:
        Serial.println("Could not find fingerprint features");
        return 0;
      case FINGERPRINT_INVALIDIMAGE:
        Serial.println("Could not find fingerprint features");
        return 0;
      default:
        Serial.println("Unknown error");
        return 0;
    }

    // OK converted!
    p = finger.fingerFastSearch();
    if (p == FINGERPRINT_OK) {
      Serial.println("Found a print match!");
    } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
      Serial.println("Communication error");
      return 0;
    } else if (p == FINGERPRINT_NOTFOUND) {
      Serial.println("Did not find a match");
      return notfound;
    } else {
      Serial.println("Unknown error");
      return 0;
    }

    // found a match!
    Serial.print("Found ID #"); Serial.print(finger.fingerID);
    Serial.print(" with confidence of "); Serial.println(finger.confidence);
    return finger.fingerID;
  }
}

void getDataPengguna(String folder, const char* hari_matkul, const char* jam_matkul, const char* durasi_matkul, const char* kodematkul_matkul, const char* kelas_matkul) {
  String data_sd_card, response_request_pengguna, bagian, link_api;
  const char* param_nim_or_nip;
  const char* response_nim;
  const char* response_nama;
  const char* response_finger1;
  const char* response_finger2;
  short j, k, counter_pengguna, count, last_counter, offset;
  byte count_file, l;

  uint16_t size_response = 25000;
  DynamicJsonDocument response_request(size_response);
  response_request = httpGetRequest(size_response, server, address_url, "/pengguna/countermatkul/" + String(kodematkul_matkul) + "/" + String(kelas_matkul));

  if (response_request.isNull()) {
    Serial.println("Request Failed!");
  }
  else {
    if (response_request["status"].as<byte>() == 1) {
      counter_download = counter_download + 1;
      createDir(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul)).c_str());

      short count_dosen = response_request["counter_dosen"].as<short>();
      short count_asisten = response_request["counter_asisten"].as<short>();
      short count_pengguna = response_request["counter_pengguna"].as<short>();
      writeFile(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul) + "/0.txt").c_str(), "{\"durasi\":\"" + String(durasi_matkul) + "\",\"kode_matkul\":\"" + String(kodematkul_matkul) + "\",\"kelas\":\"" + String(kelas_matkul) + "\",\"count_dosen\":\"" + String(count_dosen) + "\",\"count_asisten\":\"" + String(count_asisten) + "\",\"count_pengguna\":\"" + String(count_pengguna) + "\"}");

      count_file = 1;
      for (l = 0; l < 3; l++) { //0 -> dosen, 1-> asisten, 2->pengguna

        if (l == 0) {
          bagian = "Dosen";
          link_api = "/dosen/readdosen/";
          param_nim_or_nip = "nip";
          count = count_dosen;
        }
        else if (l == 1) {
          bagian = "Asisten";
          link_api = "/pengguna/readasisten/";
          param_nim_or_nip = "nip";
          count = count_asisten;
        }
        else {
          bagian = "Pengguna";
          link_api = "/pengguna/readpengguna/";
          param_nim_or_nip = "nim";
          count = count_pengguna;
        }

        offset = 0;
        //mengambil data dosen
        counter_pengguna = count / 5;
        if (count % 5 == 0) {
          counter_pengguna = counter_pengguna;
        }
        else {
          counter_pengguna = counter_pengguna + 1;
          last_counter = count % 5;
        }

        if (counter_pengguna == 0) {
          Serial.println("Data " + bagian + " " + String(kodematkul_matkul) + " Tidak ditemukan");
        }
        else {
          for (j = 1; j < (counter_pengguna + 1); j++) {
            writeFile(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul) + "/" + String(count_file) + ".txt").c_str(), "{\"hasil\":[");

            response_request = httpGetRequest(size_response, server, address_url, link_api + String(kodematkul_matkul) + "/" + String(kelas_matkul) + "/" + String(offset));

            if (response_request.isNull()) {
              counter_download = counter_download - 1;
              Serial.println("Request Failed!");
            }
            else {
              if (response_request["status"].as<byte>() == 1) {
                if ((j == counter_pengguna) && (count % 5 != 0)) {
                  for (k = 0; k < last_counter; k++) {
                    response_nim = response_request["hasil"][k][param_nim_or_nip].as<const char*>();
                    response_nama = response_request["hasil"][k]["nama"].as<const char*>();
                    response_finger1 = response_request["hasil"][k]["finger1"].as<const char*>();
                    response_finger2 = response_request["hasil"][k]["finger2"].as<const char*>();
                    data_sd_card = "{\"nim\":\"" + String(response_nim) + "\",\"nama\":\"" + String(response_nama) + "\",\"finger1\":\"" + String(response_finger1) + "\",\"finger2\":\"" + String(response_finger2) + "\"},";
                    appendFile(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul) + "/" + String(count_file) + ".txt").c_str(), data_sd_card.c_str());
                  }
                }
                else {
                  for (k = 0; k < 5; k++) {
                    response_nim = response_request["hasil"][k][param_nim_or_nip].as<const char*>();
                    response_nama = response_request["hasil"][k]["nama"].as<const char*>();
                    response_finger1 = response_request["hasil"][k]["finger1"].as<const char*>();
                    response_finger2 = response_request["hasil"][k]["finger2"].as<const char*>();
                    data_sd_card = "{\"nim\":\"" + String(response_nim) + "\",\"nama\":\"" + String(response_nama) + "\",\"finger1\":\"" + String(response_finger1) + "\",\"finger2\":\"" + String(response_finger2) + "\"},";
                    appendFile(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul) + "/" + String(count_file) + ".txt").c_str(), data_sd_card.c_str());
                  }
                  offset = offset + 5;
                }
                appendFile(SD, ("/" + folder + "/" + String(hari_matkul) + "_" + String(jam_matkul) + "/" + String(count_file) + ".txt").c_str(), "]}");
              }
              else {
                Serial.println("Failed to Get");
              }
            }
            count_file = count_file + 1;
          }
        }
      }
    }
    else {
      Serial.println("Failed to Get");
    }
  }
}

void matkulTambahan(const char* kodematkul_tambah, const char* kelas_tambah, const char* durasi_tambah) {
  byte k = 0;
  //input durasi,kodematkul,kelas
  Serial.println(kodematkul_tambah);
  Serial.println(kelas_tambah);
  Serial.println(durasi_tambah);
  DateTime now = getTime();
  String date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day());
  String postdate = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second());
  byte jam_kelas_tambahan;
  byte hari_kelas_tambahan;
  uint16_t size_response = 256;
  DynamicJsonDocument response_request(size_response);

  if (now.minute() > 40) {
    jam_kelas_tambahan = now.hour() + 1;
  }
  else {
    jam_kelas_tambahan = now.hour();
  }
  hari_kelas_tambahan = atoi(daysOfTheWeek[now.dayOfTheWeek()]);

  counter_download = 0;
  if ((checkFile(SD, "/data/99_99/0.txt") == false) || (checkFile(SD, "/data/99_99/checktime.txt") == false)) {
    lcd.setCursor(0, 1);
    lcd.print("--------------------");
    lcd.setCursor(0, 2);
    lcd.print("  Downloading Data  ");
    lcd.setCursor(0, 3);
    lcd.print("KEEP THIS DEVICE ON!");
    getDataPengguna("data", "99", "99", durasi_tambah, kodematkul_tambah, kelas_tambah);
  }
  else {
    String read_kelas_tambahan = readFile(SD, "/data/99_99/0.txt");
    deserializeJson(response_request, read_kelas_tambahan);
    const char* kodematkul_kelas_tambahan = response_request["kode_matkul"].as<const char*>();
    const char* kelas_kelas_tambahan = response_request["kelas"].as<const char*>();
    const char* durasi_kelas_tambahan = response_request["durasi"].as<const char*>();
    // pengecekan kode matkul dan kelas sama atau engga
    if ((kodematkul_kelas_tambahan != kodematkul_tambah) || (kelas_kelas_tambahan != kelas_tambah) || (durasi_kelas_tambahan != durasi_tambah)) {
      k = 0;
      while (checkFile(SD, ("/data/99_99/" + String(k) + ".txt").c_str())) {
        deleteFile(SD, ("/data/99_99/" + String(k) + ".txt").c_str());
        k = k + 1;
      }
      removeDir(SD, "/data/99_99");
      lcd.setCursor(0, 1);
      lcd.print("--------------------");
      lcd.setCursor(0, 2);
      lcd.print("  Downloading Data  ");
      lcd.setCursor(0, 3);
      lcd.print("KEEP THIS DEVICE ON!");
      getDataPengguna("data", "99", "99", durasi_tambah, kodematkul_tambah, kelas_tambah);
    }
    else {
      response_request = httpPostRequest(size_response, server, address_url, "/matkul/tambahan/create", ("waktu=" + postdate + "&kodematkul=" + String(kodematkul_tambah) + "&kelas=" + String(kelas_tambah) + "&hari=" + String(hari_kelas_tambahan) + "&jam=" + String(jam_kelas_tambahan) + "&durasi=" + String(durasi_tambah) + "&koderuangan=" + kode_ruangan));
      if (response_request.isNull()) {
        Serial.println("Request Failed!");
      }
      else {
        if (response_request["status"].as<byte>() == 1) {
          deleteFile(SD, "/data/99_99/checktime.txt");
          writeFile(SD, "/data/99_99/checktime.txt", "{\"date\":\"" + date + "\",\"hour\":" + String(jam_kelas_tambahan) + "}");
          kelasSekarang();
        }
        else {
          Serial.println("Update Failed");
        }
      }

    }
  }
  if (counter_download == 1) {
    response_request = httpPostRequest(size_response, server, address_url, "/matkul/tambahan/create", ("waktu=" + postdate + "&kodematkul=" + String(kodematkul_tambah) + "&kelas=" + String(kelas_tambah) + "&hari=" + String(hari_kelas_tambahan) + "&jam=" + String(jam_kelas_tambahan) + "&durasi=" + String(durasi_tambah) + "&koderuangan=" + kode_ruangan));
    if (response_request.isNull()) {
      Serial.println("Request Failed!");
    }
    else {
      if (response_request["status"].as<byte>() == 1) {
        writeFile(SD, "/data/99_99/checktime.txt", "{\"date\":\"" + date + "\",\"hour\":" + String(jam_kelas_tambahan) + "}");
        kelasSekarang();
      }
      else {
        Serial.println("Update Failed");
      }
    }
  }
  else {
    lcd.setCursor(0, 1);
    lcd.print("--------------------");
    lcd.setCursor(0, 2);
    lcd.print("  Download Failed   ");
    lcd.setCursor(0, 3);
    lcd.print("Failed get all data ");
  }
}

void storetoSDCard() {
  Serial.println("Start.");
  String response_request_matkul;
  float counter_loading_lcd;
  short i, j, k, l, loading_lcd;
  byte  m, n, p;

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Get data from server");
  lcd.setCursor(0, 2);
  lcd.print("-- Initialization --");
  counter_loading_lcd = 1;
  for (m = 0; m < 8; m++) {
    for (n = 0; n < 25; n++) {
      for ( l = 0; l < 2; l++) {
        if ((floor(counter_loading_lcd) + l - 1) < 19) {
          lcd.setCursor(floor(counter_loading_lcd) + l - 1, 3);
          lcd.write(0);
        }
      }
      counter_loading_lcd = counter_loading_lcd + 0.1;
      p = 0;
      while (checkFile(SD, ("/data2/" + String(m) + "_" + String(n) + "/" + String(p) + ".txt").c_str())) {
        deleteFile(SD, ("/data2/" + String(m) + "_" + String(n) + "/" + String(p) + ".txt").c_str());
        p = p + 1;
      }
      removeDir(SD, ("/data2/" + String(m) + "_" + String(n)).c_str());
    }
  }
  uint16_t size_response = 11000;
  DynamicJsonDocument response_request(size_response);
  response_request = httpGetRequest(size_response, server, address_url, "/pengguna/readmatkul/" + kode_ruangan);

  if (response_request.isNull()) {
    lcd.setCursor(0, 2);
    lcd.print("  Download Failed   ");
    lcd.setCursor(0, 3);
    lcd.print("Device Not Connected");
    Serial.println("Request Failed!");
    Downloaded_Data = false;
  }
  else {
    Serial.println("Request yes!");
    byte count_matkul = response_request["count"].as<byte>();
    Serial.println(count_matkul);
    if (response_request["status"].as<byte>() == 1) {
      if (count_matkul != 0) {
        loading_lcd = ceil(20 / count_matkul);
        counter_loading_lcd = 1;
        counter_download = 0;
        lcd.setCursor(0, 2);
        lcd.print("- Downloading Data -");
        lcd.setCursor(0, 3);
        lcd.print("                    ");

        for (i = 0; i < count_matkul; i++) {
          if (i != counter_download) {
            break;
          }
          //        Serial.println(String(response_request["hasil"][i]["hari"].as<byte>()).c_str());
          //        Serial.println(String(response_request["hasil"][i]["jam"].as<byte>()).c_str());
          //        Serial.println(String(response_request["hasil"][i]["durasi"].as<byte>()).c_str());
          //        const char* hari_matkul = String(response_request["hasil"][i]["hari"].as<byte>()).c_str();
          //        const char* jam_matkul = String(response_request["hasil"][i]["jam"].as<byte>()).c_str();
          //        const char* durasi_matkul = String(response_request["hasil"][i]["durasi"].as<byte>()).c_str();
          const char* kodematkul_matkul = response_request["hasil"][i]["kodematkul"].as<const char*>();
          const char* kelas_matkul = response_request["hasil"][i]["kelas"].as<const char*>();
          for ( l = 0; l < (loading_lcd + 1); l++) {
            lcd.setCursor(floor(counter_loading_lcd) + l - 1, 3);
            if ((floor(counter_loading_lcd) + l - 1) < 20) {
              lcd.write(0);
            }
          }
          counter_loading_lcd = counter_loading_lcd + float(20.0 / float(count_matkul));

          getDataPengguna("data2", String(response_request["hasil"][i]["hari"].as<byte>()).c_str(), String(response_request["hasil"][i]["jam"].as<byte>()).c_str(), String(response_request["hasil"][i]["durasi"].as<byte>()).c_str(), kodematkul_matkul, kelas_matkul);
          Serial.println("Updating data " + String(i + 1) + "/" + String(count_matkul));
        }
        if (count_matkul == counter_download) {
          lcd.setCursor(0, 2);
          lcd.print("  Download success  ");
          lcd.setCursor(0, 3);
          lcd.print("                    ");
          delay(2000);
          //menghapus semua file atau mereset sd card
          lcd.setCursor(0, 1);
          lcd.print("  Installing data   ");
          lcd.setCursor(0, 2);
          lcd.print("KEEP THIS DEVICE ON!");
          delay(1000);
          counter_loading_lcd = 1;
          for (i = 0; i < 8; i++) {
            for (j = 0; j < 25; j++) {
              for ( l = 0; l < 2; l++) {
                if ((floor(counter_loading_lcd) + l - 1) < 19) {
                  lcd.setCursor(floor(counter_loading_lcd) + l - 1, 3);
                  lcd.write(0);
                }
              }
              counter_loading_lcd = counter_loading_lcd + 0.1;
              k = 0;
              while (checkFile(SD, ("/data/" + String(i) + "_" + String(j) + "/" + String(k) + ".txt").c_str())) {
                deleteFile(SD, ("/data/" + String(i) + "_" + String(j) + "/" + String(k) + ".txt").c_str());
                k = k + 1;
              }
              removeDir(SD, ("/data/" + String(i) + "_" + String(j)).c_str());
            }
          }
          k = 0;
          while (checkFile(SD, ("/data/99_99/" + String(k) + ".txt").c_str())) {
            deleteFile(SD, ("/data/99_99/" + String(k) + ".txt").c_str());
            k = k + 1;
          }
          deleteFile(SD, "/data/99_99/checktime.txt");
          removeDir(SD, "/data/99_99");
          removeDir(SD, "/data");
          renameFile(SD, "/data2", "/data");
          createDir(SD, "/data2");
          lcd.setCursor(19, 3);
          lcd.write(0);
          delay(2000);
          lcd.setCursor(0, 3);
          lcd.print("---- INSTALLED! ----");
          delay(2000);
          Downloaded_Data = true;
        }
        else {
          lcd.setCursor(0, 2);
          lcd.print("  Download Failed   ");
          lcd.setCursor(0, 3);
          lcd.print("Failed get all data ");
          delay(2000);
          Downloaded_Data = false;
        }
      }
      else {
        lcd.setCursor(0, 2);
        lcd.print("   No Data Found    ");
        delay(2000);
        Downloaded_Data = true;
      }
    }
    else {
      lcd.setCursor(0, 2);
      lcd.print("  Download Failed   ");
      lcd.setCursor(0, 3);
      lcd.print(" Failed to get data ");
      Serial.println("Failed to get!");
      delay(2000);
      Downloaded_Data = false;
    }
  }
  lcd.clear();
  Serial.println("Done.");
}

void kelasSekarang() {
  Serial.println("Kelas Sekarang");
  byte jam_sekarang = 5;
  byte hari_sekarang;
  byte durasi_sekarang;

  DateTime now = getTime();
  String date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day());

  String date_kelas_tambahan = readFile(SD, "/data/99_99/checktime.txt");
  uint16_t size_response = 200;
  DynamicJsonDocument response_request(size_response);
  deserializeJson(response_request, date_kelas_tambahan);

  const char* response_date_kelastambahan = response_request["date"].as<const char*>();
  byte response_hour_kelastambahan = response_request["hour"].as<byte>();
  byte durasi_kelastambahan = parse_json_durasi("/data/99_99/0.txt");

  if ((date == response_date_kelastambahan) && (now.hour() >= response_hour_kelastambahan - 1) && (response_hour_kelastambahan + durasi_kelastambahan - 1 >= now.hour())) {
    uploadTemplateFingerprint("/data/99_99");
    next_kelas = response_hour_kelastambahan + durasi_kelastambahan - 1;
    check_next_kelas();
    speedBuzzer(500, 1);
    getFinger = true;
  }

  else {
    hari_sekarang = atoi(daysOfTheWeek[now.dayOfTheWeek()]);
    while (jam_sekarang < 24) {
      if (checkDir(SD, ("/data/" + String(hari_sekarang) + "_" + String(jam_sekarang)).c_str())) {
        durasi_sekarang = parse_json_durasi(("/data/" + String(hari_sekarang) + "_" + String(jam_sekarang) + "/0.txt").c_str());
        if ((jam_sekarang <= now.hour()) && (jam_sekarang + durasi_sekarang - 1 == now.hour()) && (now.minute() < 40)) {
          break;
        }
        else if ((jam_sekarang <= now.hour()) && (jam_sekarang + durasi_sekarang - 1 > now.hour())) {
          break;
        }
      }
      jam_sekarang = jam_sekarang + 1;
    }

    if (jam_sekarang == 24) {
      next_kelas = now.hour();
      check_next_kelas();
      getFinger = false;
      kode_matakuliah = "------";
    }
    else {
      uploadTemplateFingerprint("/data/" + String(hari_sekarang) + "_" + String(jam_sekarang));
      next_kelas = jam_sekarang + durasi_sekarang - 1;
      check_next_kelas();
      speedBuzzer(500, 1);
      getFinger = true;
    }
  }
  Serial.println(hari_sekarang);
  Serial.println(jam_sekarang);
  Serial.println(next_kelas);

  showBatreMatkul();
}

void pingDeviceAwal() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Checking for update ");
  delay(2000);
  uint16_t size_response = 256;
  DynamicJsonDocument response_request(size_response);
  response_request = httpPostRequest(size_response, server, address_url, "/ruangan/update_last_seen", ("kodedevice=" + kode_device));
  if (response_request.isNull()) {
    Serial.println("Request Failed!");
  }
  else {
    if (response_request["status"].as<byte>() == 1) {
      token = response_request["token"].as<String>();
      lcd.setCursor(0, 2);
      lcd.print("Device is up to date");
      delay(2000);
      Serial.println("update lastseen");
    }
    else if (response_request["status"].as<byte>() == 2) {
      token = response_request["token"].as<String>();
      lcd.setCursor(0, 0);
      lcd.print(" Update data found! ");
      delay(2000);
      lcd.setCursor(0, 1);
      lcd.print("    Do you want     ");
      lcd.setCursor(0, 2);
      lcd.print("   to update now?   ");
      lcd.setCursor(0, 3);
      lcd.print(" 1: Ya        2: No ");

      char keypressed = '0';
      byte starttime = start_time_second();
      byte endtime = starttime + 10;
      while (1) {
        keypressed = myKeypad.getKey();
        DateTime now = getTime();
        if (keypressed != NO_KEY) {
          if (keypressed == '1') {
            break;
          }
          else if (keypressed == '2') {
            break;
          }
        }
        if (endtime > 48) {
          starttime = now.second();
        }
        else {
          starttime = start_time_second();
        }
        if (starttime >= endtime) {
          keypressed = '1';
          break;
        }
      }
      //      char keypressed = '1';
      if (keypressed == '1') {
        storetoSDCard();
        if (Downloaded_Data == false) {
          Serial.println("Download data failed");
        }
        else {
          uint16_t size_response = 256;
          DynamicJsonDocument response_request(size_response);
          response_request = httpPostRequest(size_response, server, address_url, "/ruangan/update_device", ("kodedevice=" + kode_device + "&kode=1"));
          lcd.clear();
          if (response_request.isNull()) {
            Serial.println("Request Failed!");
          }
          else {
            if (response_request["status"].as<byte>() == 1) {
              Serial.println("updated");
            }
            else {
              Serial.println("Update Failed");
            }
          }
        }
      }
      else {
        lcd.setCursor(0, 0);
        lcd.print("  This device will  ");
        lcd.setCursor(0, 1);
        lcd.print(" update when there  ");
        lcd.setCursor(0, 2);
        lcd.print("    is no class     ");
        lcd.setCursor(0, 3);
        lcd.print("    for 2 hours     ");
        delay(4000);
      }
    }
    else {
      Serial.println("Update Failed");
    }
  }
}

void getDataFromServer_first() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Checking Connection ");
  delay(3000);
  uint16_t size_response = 256;
  DynamicJsonDocument response_request(size_response);

  if ((Ethernet.linkStatus() == LinkON) || (WiFi.status() == WL_CONNECTED)) {
    lcd.setCursor(0, 2);
    lcd.print("  Device Connected  ");
    delay(2000);

    response_request = httpPostRequest(size_response, server, address_url, "/ruangan/update_last_seen", ("kodedevice=" + kode_device));
    if (response_request.isNull()) {
      lcd.setCursor(0, 2);
      lcd.print("  Download Failed   ");
      lcd.setCursor(0, 3);
      lcd.print("  Connection Lost   ");
      Serial.println("Request Failed!");
    }
    else {
      if (response_request["status"].as<byte>() != 0) {
        token = response_request["token"].as<String>();
        storetoSDCard();
      }
      else {
        Serial.println("Update Failed");
      }
    }

    if (Downloaded_Data == false) {
      Serial.println("Download data failed");
    }
    else {
      writeFile(SD, "/kode_ruangan.txt", kode_ruangan);
    }
  }
  else {
    lcd.setCursor(0, 2);
    lcd.print("Device Not Connected");
    delay(3000);
  }
}

void showBatreMatkul() {
  int checkBaterai = 0;
  checkBaterai = analogRead(indikator_baterai);
  lcd.init();
  lcd.createChar(0, customLoading);
  lcd.createChar(2, customBaterai20);
  lcd.createChar(3, customBaterai40);
  lcd.createChar(4, customBaterai60);
  lcd.createChar(5, customBaterai80);
  lcd.createChar(6, customBaterai100);
  lcd.createChar(7, customCharging);

  lcd.setCursor(0, 3);
  if (checkBaterai > 2440) { //charging
    lcd.write(7);
  }
  else if (checkBaterai > 2330) { //4 v
    lcd.write(6);
  }
  else if (checkBaterai > 2274) { //3.85v
    lcd.write(5);
  }
  else if (checkBaterai > 2170) { //3.7v
    lcd.write(4);
  }
  else if (checkBaterai > 2087) { //3.55v
    lcd.write(3);
  }
  else { //3.4v
    lcd.write(2);
  }
  lcd.print(" " + kode_matakuliah + "  " + next_kode_matakuliah);
  delay(1000);
  //    lcd.noBacklight();
  //    esp_deep_sleep_start();
}
